package com.example.nada.zoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RegisterTest2Activity extends AppCompatActivity implements View.OnClickListener {

    private TextView signInLink;
    private Button emailRegisterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_test2);
        initControl();
    }

    private void initControl() {
        signInLink = (TextView) findViewById(R.id.from_register_to_login_link);
        signInLink.setOnClickListener(this);
        emailRegisterButton = (Button) findViewById(R.id.register_email);
        emailRegisterButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.from_register_to_login_link:
                Intent intent = new Intent(this, RegisterTestActivity.class);
                startActivity(intent);
                break;
            case R.id.register_email:
                Intent emailRegistrationIntent = new Intent(this, EmailRegistrationActivity.class);
                startActivity(emailRegistrationIntent);
                break;
        }
    }
}
