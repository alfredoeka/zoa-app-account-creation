package com.example.nada.zoa;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;


public class routine_Activity extends AppCompatActivity {

    AlarmManager remind_manager;
    TimePicker remind_timepicker;
    TextView ask_text;
    Context context;
    PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routine_);
        this.context = this;

        remind_manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        remind_timepicker = (TimePicker) findViewById(R.id.remind_timepicker);
        ask_text = (TextView) findViewById(R.id.ask_text);
        final Calendar calendar = Calendar.getInstance();
        Button turn_on_remind = (Button) findViewById(R.id.turn_on_remind);

        final Intent intentreceiver = new Intent(this.context, Reminder_Receiver.class);

        turn_on_remind.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                calendar.set(Calendar.HOUR_OF_DAY, remind_timepicker.getCurrentHour());
                calendar.set(Calendar.MINUTE, remind_timepicker.getCurrentMinute());

                int hour = remind_timepicker.getCurrentHour();
                int minute = remind_timepicker.getCurrentMinute();

                String hour_String = String.valueOf(hour);
                String minute_String = String.valueOf(minute);

                if (hour>12) {
                    hour_String = String.valueOf(hour-12);
                }
                if (minute<10){
                    minute_String = "0"+String.valueOf(minute);
                }

                set_alarm_text("Remind Feed Set to " +hour_String +":"+minute_String);

                intentreceiver.putExtra("extra","remind on");

                pendingIntent = PendingIntent.getBroadcast(routine_Activity.this, 0, intentreceiver, PendingIntent.FLAG_UPDATE_CURRENT);

                remind_manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pendingIntent);
            }
        });

        Button turn_off_remind = (Button) findViewById(R.id.turn_off_remind);

        turn_off_remind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                set_alarm_text("Remind Feed Off");

                remind_manager.cancel(pendingIntent);

                intentreceiver.putExtra("extra","remind off");

                sendBroadcast(intentreceiver);
            }
        });

    }

    public void set_alarm_text(String output) {
        ask_text.setText(output);
    }
}
