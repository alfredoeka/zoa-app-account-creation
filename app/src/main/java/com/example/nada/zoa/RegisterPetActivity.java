package com.example.nada.zoa;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;


public class RegisterPetActivity extends Activity implements OnItemSelectedListener {
Spinner s1,s2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_pet);
        s1 = (Spinner)findViewById(R.id.spinnerkat);
        s2 = (Spinner)findViewById(R.id.spinnerras);

        ArrayAdapter adapter1 = ArrayAdapter.createFromResource(this,R.array.jenis_hewan_arrays, android.R.layout.simple_spinner_item);
        s1.setAdapter(adapter1);
        s1.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (s1.getSelectedItem().equals("Anjing")) {
            Toast.makeText(getApplicationContext(), "Ras Dipilih",
                    Toast.LENGTH_SHORT).show();

            ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this,
                    R.array.Anjing, android.R.layout.simple_spinner_item);
            s2.setAdapter(adapter2);

        }else if (s1.getSelectedItem().equals("Burung")) {
            Toast.makeText(getApplicationContext(),"Ras Dipilih",
                    Toast.LENGTH_SHORT).show();
            ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this,
                    R.array.Burung, android.R.layout.simple_spinner_dropdown_item);
            s2.setAdapter(adapter2);
        } else if (s1.getSelectedItem().equals("Iguana")){
            Toast.makeText(getApplicationContext(), "Ras Dipilih",
                    Toast.LENGTH_SHORT).show();

            ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this,
                    R.array.Iguana, android.R.layout.simple_spinner_item);
            s2.setAdapter(adapter2);
        } else if (s1.getSelectedItem().equals("Kucing")){
            Toast.makeText(getApplicationContext(), "Ras Dipilih",
                    Toast.LENGTH_SHORT).show();

            ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this,
                    R.array.Kucing, android.R.layout.simple_spinner_item);
            s2.setAdapter(adapter2);
        } else if (s1.getSelectedItem().equals("Ular")){
            Toast.makeText(getApplicationContext(), "Ras Dipilih",
                    Toast.LENGTH_SHORT).show();

            ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this,
                    R.array.Ular, android.R.layout.simple_spinner_item);
            s2.setAdapter(adapter2);
        } else if (s1.getSelectedItem().equals("Kura-kura")){
            Toast.makeText(getApplicationContext(), "Ras Dipilih",
                    Toast.LENGTH_SHORT).show();

            ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this,
                    R.array.KuraKura, android.R.layout.simple_spinner_item);
            s2.setAdapter(adapter2);
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
