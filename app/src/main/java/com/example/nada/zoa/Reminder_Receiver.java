package com.example.nada.zoa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by me on 22/11/17.
 */

public class Reminder_Receiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("We are in the receiver", "Hooray");

        String get_your_string = intent.getExtras().getString("extra");

        Log.e("What is the key ?", get_your_string);

        Intent intentServ = new Intent(context,BeepPlayingService.class);

        intentServ.putExtra("extra", get_your_string);

        context.startService(intentServ);
    }
}
