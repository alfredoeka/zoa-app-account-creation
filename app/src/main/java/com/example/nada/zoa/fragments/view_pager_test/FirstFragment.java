package com.example.nada.zoa.fragments.view_pager_test;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.nada.zoa.R;
import com.example.nada.zoa.ViewPagerExample;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

/**
 * Created by alfredo on 11/12/2017.
 */

public class FirstFragment extends android.support.v4.app.Fragment implements Step {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.first_frag, container, false);

        ImageView userImage = v.findViewById(R.id.imageUser);
        Glide.with(this).load("https://api.adorable.io/avatars/285/alfredo@gmail.com.png").into(userImage);

        ActionBar actionBar = ((ViewPagerExample)this.getActivity()).getSupportActionBar();
        actionBar.setTitle("User Information");
        return v;
    }

    public static FirstFragment newInstance(String text) {

        FirstFragment f = new FirstFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
